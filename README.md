Tugas Besar (2) IF3038
======================

Hello!

Tugas ini merupakan Tugas 4, pengembangan dari Tugas 2 atau 3 untuk membuat suatu
Web Service dengan menggunakan teknik SOAP (hanya untuk create) dan REST.

Tugas 4 kami dibuat dengan menggunakan PHP (pengembangan dari Tugas 2).

Untuk list REST API bisa buka [API.txt](https://github.com/ynov/madtodo/blob/master/API.txt)

Informasi SOAP [SOAP.md](https://github.com/ynov/madtodo/blob/master/SOAP.md)

Untuk tugas-tugas sebelumnya, bisa dilihat di branch lain:
+   [Tugas 1](https://github.com/ynov/madtodo/tree/bak-master)
+   [Tugas 2 (PHP)](https://github.com/ynov/madtodo/tree/master-php)
+   [Tugas 3 (Java)](https://github.com/ynov/madtodo/tree/master-java)

--------------------------------------------------------------------------------

Deployment: [http://madtodo.aws.af.cm/](http://madtodo.aws.af.cm/)

Validation:
[Validate HTML5](http://validator.w3.org/check?uri=http://madtodo.aws.af.cm/) |
[Validate CSS3](http://jigsaw.w3.org/css-validator/validator?uri=http://madtodo.aws.af.cm/)
