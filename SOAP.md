SOAP
====

Untuk soap, kami menggunakan native library yang disediakan oleh PHP 5 untuk SOAP
(SoapServer dan SoapClient).

SoapServer yang kami deploy di AppFog beralamat: `http://madtodo.aws.af.cm/soap/server.php`
SOAP Server kami tidak menggunakan WSDL, sehingga, untuk menggunakannya
harus diberi tahu secara manual (Salah satunya dengan membaca SOAP.md ini)

Ada beberapa fungsi yang kami sediakan, semuanya untuk proses pembuatan, atara lain:

+   `createTask()`
+   `createCategory()`
+   `createUser()`

Fungsi tersebut dapat dipanggil secara remote dengan SoapClient yang disediakan
PHP.

    $client = new SoapClient(null, array(
        'location' => 'http://madtodo.aws.af.cm/soap/server.php',
        'uri' => 'http//madtodo.aws.af.cm/soap/'
    ));

Parameter dari fungsi create* adalah data yang telah diserialisasikan ke JSON (dalam
bentuk string).

Contoh untuk memanggil `createCategory()`, data dijadikan JSON string dengan `json_encode()`

    $data = array(
        'name' => 'Category From SOAP',
        'user_id' => 1
    );

    return $client->createCategory(json_encode($data));

Format data yang diperlukan untuk setiap fungsi sama seperti yang dipakai pada REST.
