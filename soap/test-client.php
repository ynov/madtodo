<?php
$client = new SoapClient(null, array(
    'location' => 'http://localhost:8960/soap/test-server.php',
    'uri' => 'http//localhost:8960/soap/'
));

header('Content-Type: text/plain');

// test add
$a = 40; $b = 27;
echo "$a + $b = " . $client->add($a, $b) . "\n";

// test testJson
$json = json_encode(array('foo' => 'bar qux'));
echo $client->testJson($json) . "\n";
