<?php
ini_set('soap.wsdl_cache_enabled', '0');

$server = new SoapServer(null, array('uri' => 'http://localhost:8960/soap/'));

$server->addFunction('add');
$server->addFunction('testJson');

$server->handle();

function add($a, $b) {
    return $a + $b;
}

function testJson($json) {
    $arr = json_decode($json, true);
    return 'Your "foo" = ' . $arr['foo'];
}
